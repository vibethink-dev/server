'use strict';

var fs = require('fs'),
    data = JSON.parse(fs.readFileSync('./data.json.bak')),
    polls = data.polls;

    data.votes = {};

Object.keys(polls).forEach(function (pollId) {
  var poll = polls[pollId];

  data.votes[pollId] = {
    "0": poll.categories
  };

  poll.round = 0;
  poll.rounds = {
    "0": {
      id: 0,
      name: 'Nominate',
      nextCategoryId: poll.nextCategoryId,
      allowOther: true,
      displayPercent: true,
      displayCount: false,
    }
  };

  delete poll.nextCategoryId;
  delete poll.categories;
});

fs.writeFileSync('./data.json', JSON.stringify(data, null, ' '));
console.log(data);