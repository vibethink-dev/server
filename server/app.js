// 'use strict';

require('sugar')();

var fs     = require('fs'),
    config = require('../config.js'),
    server = require('http').createServer(),
    socketio = require('socket.io'),
    io     = socketio(server),
    util   = require('util'),
    net    = require('net'),
    repl   = require('repl'),
    votedIP = {},
    voteCount = 0,
    stats = {
      connected: 0
    },
    functions = require('./functions.js'),
    dataFiles = [],
    extendedSocket = false,
    backup = function (data) {
      if (voteCount % 100 !== 0) {
        return;
      }

      // var file = config.backupDir + (new Date()).getTime() + '.json';
      // console.log('Writing backup file to ' + file);

      // fs.writeFileSync(file, JSON.stringify(data));
    };


Object.keys(functions.events).forEach(function (key) {
  var args = [];

    functions.events[key].toString().replace(/^([^\{]+?\{).*/, '$1')
      .replace(/\(([^\)]*)\)/, function (m, a) {
        if (a.length > 0) {
          args.append(a.split(/\,\s*/g));
        }
      });

    args.append(functions.events[key].toString()
      .replace(/^[^\{]+?\{/, 'try {\nthis.lastEvent = "' + key + '";' +
      (args.length > 0 ? 'this.logEvent("' + key + '", { ' + args.map(function (arg) {
          return [JSON.stringify(arg), arg].join(': ');
        }).join(', ') + ' });' : ''))
    .replace(/\}[^\}]*$/, '} catch (err) { this.logEvent("error", err.stack); this.emit("err", err.stack); }'));

    functions.events[key] = Function.apply(null, args);
  });

fs.readFile(config.dataFile, function (err, fileData) {
  if (err) throw err;

  var data = JSON.parse(fileData),
      votes = data.votes,
      polls = data.polls,
      emitPollGlobal = function (poll) {
        try {
          if (!poll || poll.id === undefined) {
            console.error('No poll or poll.id, got ' + JSON.stringify(poll));
          }

          io.in(poll.id).emit('poll', poll);
          // console.log(JSON.stringify(poll));
          fs.writeFileSync(config.dataFile, JSON.stringify(data, null, '  '));
          backup(data);
          console.log('Emitting global data. Writing new data to ' + config.dataFile);
        } catch (err) {
          console.error(err.stack);
        }
      }.debounce(10),
      emitVotesGlobal = function (poll) {
        try {
          if (!poll || poll.id === undefined) {
            console.error('No poll or poll.id, got ' + JSON.stringify(poll));
          }

          io.in(poll.id).emit('poll', poll);
          // console.log(JSON.stringify(poll));
          fs.writeFileSync(config.dataFile, JSON.stringify(data, null, '  '));
          backup(data);
          console.log('Emitting global data. Writing new data to ' + config.dataFile);
        } catch (err) {
          console.error(err.stack);
        }
      }.debounce(10);

  dataFiles.push(data);

  io.on('connection', function (socket) {
    stats.connected++;

    socket.ipAddress = socket.handshake.address || socket.handshake.headers['x-real-ip'];

    if (extendedSocket === false) {
      extendedSocket = true;
      extendSocket(Object.getPrototypeOf(socket));
    }

    // log('Client connected: ' + socket.ipAddress);

    if (socket._events === undefined) {
      socket._events = {};
    }
    Object.keys(functions.events).forEach(function (key) {
      socket._events[key] = functions.events[key];
      socket._eventsCount++;
    });

    socket.logEvent('connected', stats);

    socket.on('disconnect', function () {
      stats.connected--;
    });
  });

  function extendSocket(Socket) {
    Object.keys(functions).forEach(function (key) {
      Socket[key] = functions[key];
    });

    Socket.votes = votes;
    Socket.polls = polls;
    Socket.emitPollGlobal = function (poll) {
      io.in(poll.id).emit('poll', poll);
    }
    Socket.emitVotesGlobal = function (votes) {
      io.in(this.poll.id).emit('votes', votes);
    }
  }
});

function sanitizeName(string) {
  return string.toLowerCase().replace(/[^a-z]+/g, '');
}

server.listen(config.server.port, function () {
  log('Listening on ' + config.server.port);
});

net.createServer(function (socket) {
  var r = repl.start({
    prompt: "server > ",
    input: socket,
    output: socket,
    terminal: true,
    useGlobal: true
  });

  r.context.data = dataFiles;

  socket.on('close', function () {
    r.close();
  });
}).listen(config.server.repl.port);

log('REPL server started on port ' + config.server.repl.port + '. Connect using `npm run repl`.');

function log(str) {
  if (config.debug === true) {
    console.log(str);
  }
}