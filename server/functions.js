'use strict';

require('sugar')();

var uuid = require('uuid/v1'),
    util = require('util'),
    colors = require('colors');

function getUnixTimestamp() {
  return Math.round((new Date()).getTime() / 1000);
}

function expired(timestamp, start) {
  if (typeof timestamp === 'string') {
    return expired(parseInt(timestamp, 10));
  }

  if (!timestamp) {
    return false;
  }

  if (start === true) {
    return getUnixTimestamp() < timestamp;
  }

  return getUnixTimestamp() > timestamp;
}

module.exports = {
  events: {
    'user:login': function (token) {
      if (this.authenticated = (token === 'fYRXfHdCocRENTvFz2WUWZYY')) {
        this.emit('authenticated', true);
      }
    },
    'user:logout': function () {
      if (this.authenticated === true) {
        delete this.authenticated;
        this.emit('authenticated', false);
      }
    },
    'candidate:vote': function (categoryId, name) {
      var self = this;

      this.getCategory(categoryId, function (category) {
        self.getCandidates(category, function (candidates) {
            self.setCandidate(candidates, name, 1);
          });
      });
    },
    'candidate:add': function (categoryId, name) {
      var self = this;

      self.check(self.authenticated, function () {
        self.check(name, function () {
          self.getCategory(categoryId, function (category) {
            self.getCandidates(category,function (candidates) {
              self.setCandidate(candidates, name, 0);
            });
          });
        }, 'Invalid candidate name ' + JSON.stringify(name));
      }, 'You must log in to add a candidate');
    },
    'candidate:rename': function (categoryId, oldName, newName) {
      var self = this;

      this.getCategory(categoryId, function (category) {
        self.getCandidates(category,
          self.emitVotesOnTrue(function (candidates) {
            return self.removeCandidate(candidates, oldName, function (votes) {
              return self.setCandidate(candidates, newName, votes);
            });
          }));
      });
    },
    'candidate:remove': function (categoryId, name) {
      var self = this;

      self.getCategory(categoryId, function (category) {
        self.getCandidates(category,
          self.emitVotesOnTrue(function (candidates) {
            return self.removeCandidate(candidates, name, function (votes) {
              console.log('Removed candidate ' + JSON.stringify(name) + ' with ' + votes + ' votes');
            });
          }));
      });
    },
    'category:add': function (name) {
      var self = this;

      self.check(name, function () {
        self.getVotes(self.emitVotesOnTrue(function (categories) {
          return self.newCategory(name, function (category) {
            categories.append(category);
          });
        }));
      }, 'Invalid category name ' + JSON.stringify(name));
    },
    'category:rename': function (categoryId, name) {
      var self = this;

      self.getCategory(categoryId, self.emitVotesOnTrue(function (category) {
        if (category.name === name) {
          return false;
        }

        return (category.name = name);
      }));
    },
    'category:remove': function (categoryId) {
      var self = this;

      self.getCategory(categoryId, self.emitVotesOnTrue(function (category, categories) {
        return categories.remove(category);
      }));
    },
    'poll:set': function (pollId) {
      var self = this;

      self.getPoll(pollId, function (poll) {
        self.poll = poll;
        self.join(poll.id);
  
        self.emitPoll();
        self.getRound(function () {
          self.emitVotes();
        });
      });
    },
    'round:set': function (roundId) {
      var self = this;

      self.check(self.authenticated, function () {
        self.getRound(roundId, self.emitPollOnTrue(self.emitVotesOnTrue(function (round) {
          if (self.roundId === round.id) {
            return false;
          }

          self.emit('round:set', round);
          return (self.roundId = round.id);
        })));
      }, 'You must log in to set the current round ID');
    },
    'round:update': function (round) {
      var self = this;

      self.check(self.authenticated, function () {
        self.check(round, function () {
          self.check(round.id, function () {
            self.check(round.name, function () {
              self.getRound(round.id, function (targetRound) {
                if (typeof round.categories === 'object') {
                  self.getVotes(round.id, function (categories) {
                    self.setCategories(targetRound, round.categories, categories);
                    delete round.categories;
                  });
                }

                Object.merge(targetRound, round);
                self.emitPoll();
              });
            }, 'Invalid round name ' + JSON.stringify(round.name));
          }, 'Invalid round ID ' + JSON.stringify(round.id));
        }, 'Invalid round object, got ' + JSON.stringify(round));
      }, 'You must log in to update round information');
    }
  },
  check: function (value, callback, error, types) {
    if (!(value instanceof Function) && (value === 0 || value) &&
        (types === undefined || types.indexOf(typeof value) !== -1)) {
      return callback(value);
    } else if (error instanceof Function) {
      return error(callback);
    } else {
      this.emitError(error);
      return false;
    }
  },
  setCategories: function (round, source, target) {
    var self = this;

    Object.keys(source).forEach(function (categoryName) {
      var category = target.find({ name: categoryName });

      if (!category) {
        self.newCategory(categoryName, function (category) {
          self.setCandidates(category, source[categoryName]);
          target.append(category);
        });
      } else {
        self.setCandidates(category, source[categoryName]);
      }
    });
  },
  getCategory: function (categoryId, callback) {
    var self = this;

    self.getVotes(function (categories) {
      self.check(
        categories.find({ id: parseInt(categoryId, 10) }), function (category) {
          callback(category, categories);
        }, 'Category not found');
    });
  },
  newCategory: function (name, callback) {
    var self = this;

    self.getRound(function (round) {
      self.check(round.nextCategoryId, function () {
        callback({
          id: round.nextCategoryId++,
          name: name
        });
      }, 'Next category ID not found for this round');
    });
  },
  getCandidates: function (category, callback) {
    return this.check(category.candidates, function (candidates) {
      return callback(candidates);
    }, function (retry) {
      return retry(category.candidates = {});
    });
  },
  setCandidate: function (candidates, name, increment) {
    var self = this;

    return this.check(candidates[name], function () {
      candidates[name] += increment;
      self.emitVotes();
    }, function () {
      self.getRound(function (round) {
        self.check(round.allowOther !== false ||
                   self.authenticated, function () {
          candidates[name] = increment;
          self.emitVotes();
        }, 'Other candidates not allowed for this round');
      });
    });
  },
  setCandidates: function (category, candidates) {
    var self = this;

    category.candidates = category.candidates || {};

    this.check(candidates, function () {
      candidates.forEach(function (name) {
        self.setCandidate(category.candidates, name, 0);
      });
    }, 'Candidates not defined');
  },
  removeCandidate: function (candidates, name, callback) {
    return this.check(candidates[name], function (votes) {
      return (delete candidates[name]) && callback(votes);
    }, 'Candidate ' + JSON.stringify(name) + ' not found');
  },
  getPoll: function (pollId, callback) {
    var self = this;

    this.check(pollId, function (pollId) {
      self.check(self.polls[pollId], function (poll) {
          if (poll.id !== pollId) {
            poll.id = pollId;
          }

          if (!poll.rounds) {
            poll.rounds = {};
          }

          self.updateCurrentRound(poll);

          callback(poll);
        },
        self.authenticated === true
          ? function (retry) {
              retry(self.newPoll(pollId));
              self.emitPoll();
            }
          : 'No poll found for id ' + JSON.stringify(pollId) + ', please login to create one');
    }, function () {
      self.check(self.poll, pollId, 'No poll associated with this socket');
    });
  },
  newPoll: function (pollId) {
    var self = this;

    return self.check(pollId, function () {
      return self.polls[pollId] = {
          id: pollId,
          rounds: {}
        };
      }, 'Invalid poll ID, got ' + JSON.stringify(pollId));
  },
  getRound: function (roundId, callback) {
    var self = this;

    if (roundId instanceof Function) {
      return this.getRound(this.roundId, roundId);
    }

    this.getPoll(function (poll) {
      self.check(poll.rounds, function (rounds) {
        self.check(roundId, function (roundId) {
            self.check(rounds[roundId], function (round) {
              self.check(
                self.lastEvent !== 'candidate:vote' ||
                !expired(round.votingEnds),
                function () {
                  self.updateCurrentRound(poll);
                  callback(round, poll);
                }, function () {
                  self.emitPoll();
                  self.emitVotes();
                  self.emitError('This round has been closed for voting');
                  console.log(parseInt(round.votingEnds, 10), getUnixTimestamp());
                });
            }, self.authenticated === true
                ? function (retry) {
                    self.newRound(rounds, roundId, function (round) {
                      self.roundId = round;
                      self.newVotes(poll.id, round.id, function () {
                        retry(round);
                        self.emitPoll();
                      });
                    });
                  }
                : 'No round with ID ' + JSON.stringify(roundId)
            );
        }, function (retry) {
          self.check(poll.currentRoundId, function (roundId) {
            retry(roundId);
          }, self.authenticated === true
              ? function (retry) {
                  retry(uuid());
                }
              : 'No round ID specified, please login to create one')
        }, ['string', 'number']);
      }, function (retry) {
        retry(poll.rounds = {});
      });
    });
  },
  updateCurrentRound: function (poll) {
    var currentRound = poll.rounds[poll.currentRoundId];

    if (!currentRound || expired(currentRound.votingEnds) || expired(currentRound.roundStarts, true)) {
      currentRound = Object.values(poll.rounds).filter(function (round) {
        return !expired(round.roundStarts, true);
      }).sortBy('roundStarts', true).last()
    }

    if (currentRound.id === poll.currentRoundId) {
      return;
    }

    poll.currentRoundId = currentRound.id;

    this.emitPollGlobal(poll);
  },
  newRound: function (rounds, roundId, callback) {
    return this.check(roundId, function () {
      callback(rounds[roundId] = {
          id: roundId,
          name: roundId,
          nextCategoryId: 0
        });
      }, 'Invalid round ID, got ' + JSON.stringify(roundId), ['string', 'number']);
  },
  getVotes: function (roundId, callback) {
    var self = this;

    if (roundId instanceof Function) {
      return this.getVotes(this.roundId, roundId);
    }

    this.getRound(roundId, function (round, poll) {
      self.check(self.votes[poll.id], function (rounds) {
        self.check(rounds[round.id], callback,
          self.authenticated === true
            ? function (retry) {
                self.newVotes(poll.id, roundId, retry);
              }
            : 'No round found for ' + JSON.stringify(round.id));
      }, 'No categories for this poll');
    });
  },
  newVotes: function (pollId, roundId, callback) {
    var self = this;

    self.check(self.votes[pollId], function (votes) {
      self.check(votes[roundId], function (categories) {
        callback(categories);
      }, function (retry) {
        retry(votes[roundId] = []);
      });
    }, function (retry) {
      retry(self.votes[pollId] = {});
    });
  },
  emitVotesOnTrue: function (callback) {
    var self = this;

    return function () {
      if (callback.apply(self, arguments) !== false) {
        self.emitVotes();
        return true;
      }

      return false;
    }
  },
  emitPollOnTrue: function (callback) {
    var self = this;

    return function () {
      if (callback.apply(self, arguments) !== false) {
        self.emitPoll();
        return true;
      }

      return false;
    }
  },
  emitError: function (error) {
    this.emit('err', error);
    this.logEvent('error', error)
  },
  emitPoll: function () {
    var self = this;

    self.getPoll(function (poll) {
      self.emit('poll', poll);
      self.emitPollGlobal(poll);
    });
  },
  emitVotes: function () {
    var self = this;

    // self.getVotes(function (round) {
    self.getPoll(function (poll) {
      var votes = self.votes[poll.id];
      self.emit('votes', votes);
      self.emitVotesGlobal(votes);
    });
  },
  logEvent: function (event, data) {
    var padIp = 18,
        padEvent = 18,
        eventName = event.padRight(padEvent)
          .replace(/(dis)?connect.*/, '$&'.gray)
          .replace(/poll.*/, '$&'.yellow)
          .replace(/auth.*/, '$&'.blue)
          .replace(/user.*/, '$&'.blue)
          .replace(/categ.*/, '$&'.magenta)
          .replace(/candid.*/, '$&'.green)
          .replace(/round.*/, '$&'.cyan)
          .replace(/error/, '$&'.red),
        message,
        ipAddress = this.ipAddress.padRight(padIp);

    if (this.authenticated) {
      ipAddress = ipAddress.green;
    } else {
      ipAddress = ipAddress.gray;
    }

    if (typeof data === 'string') {
      message = data.gray;
    } else {
      message = util.inspect(data, {
        colors: true
      });
    }

    console.log(
      ipAddress,
      eventName,
      message.replace(/(\n)/g, '\n' + ' '.repeat(padIp + padEvent)));
  }
};