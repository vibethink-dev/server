'use strict';

var config = require('../config.js'),
    io = require('socket.io-client'),
    socket = io.connect(config.server.url, { reconnect: true }),
    assert = require('assert'),
    throwErr = function (msg) {
      throw new Error(msg);
    },
    data = {
      category: 'testCategory'
    };

describe('poll', function() {
  describe('set', function() {
    var POLL_IDS = ['testA', 'testB'],
        matched = 0;

    it('should not work unless authenticated', function (done) {
      socket.emit('user:logout');
      socket.removeListener('err', throwErr);

      socket.once('err', function (err) {
        socket.emit('user:login', 'fYRXfHdCocRENTvFz2WUWZYY');
        socket.emit('poll:set', POLL_IDS[0]);
        socket.once('authenticated', function () {
          socket.on('err', throwErr);
          done();
        });
      });
      socket.emit('poll:set', POLL_IDS[0]);
    });

    it('should create a new poll', function (done) {
      var pollFunc = function (poll) {
        if (POLL_IDS.indexOf(poll.id) !== -1 &&
            ++matched >= POLL_IDS.length) {
          socket.removeListener('poll', pollFunc);
          done();
        }
      }

      socket.on('poll', pollFunc);
      socket.emit('poll:set', POLL_IDS[0]);
      setTimeout(function () {
        socket.emit('poll:set', POLL_IDS[1]);
      }, 300);
    });

    it('should change polls', function (done) {
      var pollFunc = function (poll) {
        if (poll.id === POLL_IDS[0]) {
          socket.removeListener('poll', pollFunc);
          done();
        }
      }

      socket.once('poll', pollFunc);
      socket.emit('poll:set', POLL_IDS[0]);
    });

    it('should work with id = 0', function (done) {
      socket.emit('poll:set', 0);

      socket.once('poll', function (poll) {
        assert.equal(poll.id, 0);
        done();
      });
    });
  });
});

describe('round', function () {
  describe('set', function () {
    var POLL_ID = 'testA',
        ROUND_ID = 'testRound';

    socket.emit('poll:set', POLL_ID);

    it('should not work unless authenticated', function (done) {
      socket.emit('user:logout');
      socket.removeListener('err', throwErr);

      socket.once('err', function (err) {
        socket.emit('user:login', 'fYRXfHdCocRENTvFz2WUWZYY');
        socket.emit('round:set', ROUND_ID);
        socket.once('authenticated', function () {
          socket.on('err', throwErr);
          done();
        });
      });

      socket.emit('round:set', ROUND_ID);
    });

    it('should create a new round', function (done) {
      socket.once('poll', function (poll) {
        var round = poll.rounds[ROUND_ID];

        assert.ok(round);
        assert.ok(Object.keys(round).length);
        assert.ok(round.id);

        done();
      });

      socket.emit('round:set', ROUND_ID);
    });

    it('should work with id = 0', function (done) {
      socket.once('poll', function (poll) {
        var round = poll.rounds[0];

        assert.ok(round);
        assert.ok(Object.keys(round).length);
        assert.strictEqual(round.id, 0);

        done();
      });

      socket.emit('round:set', 0);
    });
  });

  describe('update', function () {

  });
});

describe('category', function () {
  describe('add', function () {
    it('should not work unless authenticated', function (done) {
      socket.emit('user:logout');
      socket.removeListener('err', throwErr);

      socket.once('err', function (err) {
        socket.emit('user:login', 'fYRXfHdCocRENTvFz2WUWZYY');
        socket.once('authenticated', function () {
          socket.on('err', throwErr);
          done();
        });
      });
      socket.emit('category:add', data.category);
    });

    it('should add a category', function (done) {
      socket.emit('category:add', data.category);

      socket.once('votes', function (votes) {
        var category = votes.filter(function (category) {
          return category.id !== null && category.id !== undefined &&
                 category.name === data.category;
        })[0];

        assert.ok(category);

        data.categoryId = category.id;
        
        done();
      });
    });
  });

  describe('rename', function () {
    it('should rename a category', function (done) {
      var newName = 'newCategoryName';

      socket.once('votes', function (votes) {
        assert.ok(votes.filter(function (category) {
          return category.id !== null && category.id !== undefined &&
                 category.name === newName;
        }).length > 0);
        
        done();
      });

      socket.emit('category:rename', data.categoryId, newName);
    });
  });
});

describe('candidate', function () {
  describe('add', function () {
    var CID = 'testCandidate';

    it('should not work unless authenticated', function (done) {
      socket.emit('user:logout');
      socket.removeListener('err', throwErr);

      socket.emit('candidate:add', data.categoryId, CID);
      socket.once('err', function (err) {
        socket.on('err', throwErr);
        socket.emit('user:login', 'fYRXfHdCocRENTvFz2WUWZYY');
        done();
      });
    });
  });
});

socket.on('disconnect', function () {
  console.log('disconnected');
});
