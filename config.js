module.exports = {
  debug: true,
  server: {
    repl: {
      port: 4444
    },
    port: 8080
  },
  dataFile: __dirname + '/server/data.json',
  backupDir: __dirname + '/backups/',
  ipLog: __dirname + '/vote-log.csv'
};