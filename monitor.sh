echo -n 'Unique IPs: '
cat vote-log.csv | grep -oE '([0-9]+\.)+[0-9]+' | sort | uniq | wc -l
echo
echo -n 'Votes: '
wc -l vote-log.csv
echo
echo -n 'Backups: '
ls -1 ./backups/ | wc -l
du -hc ./backups/
echo
free -mh
echo
df -h /dev/vda1